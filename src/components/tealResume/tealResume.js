import React from 'react';
import classes from './tealResume.module.css'

import LeftBar from '../teal-leftbar/tealLeftBar';

const tealResume = (props) => (
    <div className= {classes.tealResume}>
        <form>
            <div className={classes.tealResumeInternal}>
                <div className={classes.tealResumeUpper}>
                    <LeftBar/>
                </div>
                
            </div>
        </form>
    </div>
);

export default tealResume;