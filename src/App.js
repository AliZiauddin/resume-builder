import './App.css';
import TealResume from './components/tealResume/tealResume';

function App() {
  return (
    <TealResume/>
  );
}

export default App;
