import React from 'react';
import classes from './tealLeftBar.module.css';

const leftBar = (props)=> (
    <div className = {classes.tealLeftBarHead}>
        <p className = {classes.tealResume} placeholder="Full Name" >
              &nbsp;&nbsp; Resume
            </p>
        <p className = {classes.tealFullName} placeholder="Full Name" 
            contentEditable="true" >
                &nbsp;Full Name
            </p>
        <p className = {classes.tealDesignation} placeholder="Full Name"  
            contentEditable="true" >    
                &nbsp;&nbsp; Designation
            </p>
    </div>
    );


export default leftBar;